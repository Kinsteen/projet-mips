EXEC = emul
SRCDIR = src
OBJDIR = obj
CFLAGS = -g -Wall
LFLAGS = -lm
FILE = test3.txt

SRCS=$(wildcard $(SRCDIR)/*.c)
OBJS= $(SRCS:$(SRCDIR)/%.c=$(OBJDIR)/%.o)
HEADER =$(wildcard $(SRCDIR)/*.h)


$(EXEC): $(OBJS) $(HEADER)
	gcc $(OBJS) $(LFLAGS) -o $@

$(OBJDIR)/%.o: $(SRCDIR)/%.c $(HEADER)
	gcc -c $< $(CFLAGS) -o $@

.PHONY: run debug clean

run: $(EXEC)
	./$(EXEC) $(FILE)

pas: $(EXEC)
	./$(EXEC) $(FILE) -pas

debug: $(EXEC)
	gdb --args ./$(EXEC) $(FILE)

clean : 
	@rm -rf $(EXEC)
	@rm -rf $(OBJDIR)/*.o
