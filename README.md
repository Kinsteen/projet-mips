# Projet MIPS

`make` pour compiler, puis `./emul <nom du fichier>` pour lancer le programme, avec le code assembleur MIPS donné.

Aussi possible de lancer le mode interactif, en lançant simplement `./emul`.

Le mode pas-a-pas est disponible, en utilisant la commande `./emul <nom du fichier> -pas`.

Plusieurs commandes sont disponibles dans le mode pas à pas et interactif :
 - `r` pour afficher la valeurs de tous les registres.
 - `m` pour afficher le contenu de la mémoire.
 - `c` (uniquement pour le mode pas à pas) pour continuer l'éxécution du programme.