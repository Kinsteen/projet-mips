#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <stdint.h>

#include "cpu.h"
#include "instructions.h"

// This is where code will be executed

const u_int8_t opcode_type_I[] = {
    0b001000, // ADDI
    0b100011, // LW
    0b101011, // SW
    0b001111, // LUI
    0b000100, // BEQ
    0b000101, // BNE
    0b000111, // BGTZ
    0b000110  // BLEZ
};

const u_int8_t opcode_type_R[] = {
    0b000000, // ADD
    0b000000, // SUB
    0b000000, // MULT
    0b000000, // DIV
    0b000000, // AND
    0b000000, // OR
    0b000000, // XOR
    0b000000, // ROTR
    0b000000, // SLL
    0b000000, // SRL
    0b000000, // SLT
    0b000000, // MFHI
    0b000000, // MFLO
    0b000000  // JR
};

const u_int8_t opcode_type_J[] = {
    0b000010, // J
    0b000011  // JAL
};

enum InstType getType(const int inst) {
    enum InstType result = -1;
    u_int8_t opcode = inst>>26 & 0b111111;

    if (opcode == 0) {
        result = R;
    }

    for (int i = 0; i < 8 && result == -1; i++) {
        if (opcode_type_I[i] == opcode) {
            result = I;
        }
    }
    
    for (int j = 0; j < 2 && result == -1; j++) {
        if (opcode_type_J[j] == opcode) {
            result = J;
        }
    }

    return result;
}

void executeInstruction(u_int32_t inst) {
    enum InstType type = getType(inst);

    if (type == R) {
        u_int8_t rs = inst>>21 & 0b11111;
        u_int8_t rt = inst>>16 & 0b11111;
        u_int8_t rd = inst>>11 & 0b11111;
        u_int8_t sa = inst>>6  & 0b11111;
        u_int8_t function = inst & 0b111111;

        if (function == 0b100000) {
            ADD(rd,rs,rt);
        } else if (function == 0b100010) {
            SUB(rd, rs, rt);
        } else if (function == 0b011000) {
            MULT(rs, rt);
        } else if (function == 0b011010) {
            DIV(rs, rt);
        } else if (function == 0b100100) {
            AND(rd, rs, rt);
        } else if (function == 0b100101) {
            OR(rd, rs, rt);
        } else if (function == 0b100110) {
            XOR(rd, rs, rt);
        } else if (function == 0b000010) { // ROTR ou SRL
            if (rs == 1) { // ROTR
                ROTR(rd, rt, sa);
            } else if (rs == 0) { // SRL
                SRL(rd, rt, sa);
            }
        } else if (function == 0b000000) {
            SLL(rd, rt, sa);
        } else if (function == 0b101010) {
            SLT(rd, rs, rt);
        } else if (function == 0b010000) {
            MFHI(rd);
        } else if (function == 0b010010) {
            MFLO(rd);
        } else if (function == 0b001000) {
            JR(rs);
        }
    } else if (type == I) {
        u_int8_t opcode = inst>>26 & 0b111111;
        u_int8_t rs = inst>>21 & 0b11111;
        u_int8_t rt = inst>>16 & 0b11111;
        int16_t immediate = inst;

        if (opcode == 0b001000) {
            ADDI(rt, rs, immediate);
        } else if (opcode == 0b100011) {
            LW(rt, immediate, rs);
        } else if (opcode == 0b101011) {
            SW(rt, immediate, rs);
        } else if (opcode == 0b001111) {
            LUI(rt, immediate);
        } else if (opcode == 0b000100) {
            BEQ(rs, rt, immediate);
        } else if (opcode == 0b000101) {
            BNE(rs, rt, immediate);
        } else if (opcode == 0b000111) {
            BGTZ(rs, immediate);
        } else if (opcode == 0b000110) {
            BLEZ(rs, immediate);
        }
    } else if (type == J) {
        u_int8_t opcode = inst>>26 & 0b111111;
        u_int32_t target = inst & 0x3FFFFFF;

        if (opcode == 0b000010) {
            Jfunction(target);
        } else if (opcode == 0b000011) {
            JAL(target);
        }
    } else {
        printf("Error !\n");
    }
}
