#ifndef CPU_H
#define CPU_H

#define PROG_OFFSET 100

/*

Pour les registres, nous avons la taille prédéfinie, et ils ne sont qu'au
nombre de 32, donc nous pouvons simplement allouer un tableau normal.

*/

enum InstType {
    I,
    R,
    J
};

int32_t registers[32];
u_int32_t PC; // program counter
u_int32_t HI; // HI and LO registers for MULT or DIV instruction
u_int32_t LO;

//enum InstType getType(const char* inst);
enum InstType getType(const int inst);
void executeInstruction(u_int32_t inst);

#endif