#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "parser.h"
#include "memory.h"
#include "cpu.h"

FILE* openFile(char *fileName, char *mode) {
    FILE *file = NULL;
    file = fopen(fileName, mode);

    if (file == NULL) {
        printf("Error! Could not open file\n");
        exit(-1);
    }
    
    return file;
}

void parseLabels(char *line, int max, int *l, u_int32_t *addr, FILE *output) {
    if (strstr(line, ":") != NULL) { // It's a label.
        line = strtok(line, ":");
        struct label newLabel;
        newLabel.address = *addr;
        strcpy(newLabel.labelName, line);
        labelArray[(*l)++] = newLabel;
    } else {
        *addr += 4;
    }
}

void parseInstructions(char *line, int max, int *l, u_int32_t *addr, FILE *output) {
    if (strstr(line, ":") == NULL) { // It's not a label.
        char entireInst[50];
        strcpy(entireInst, line);

        int status = 0;
        u_int32_t hexCode = parseStringToHex(line, &status, *addr); // line is modified, don't forget
        if (status == 0) {
            fprintf(stderr, "Unsupported instruction : %s, exiting.\n", line);
            exit(-1);
        }
        fprintf(output, "%08X\n", hexCode);

        writeWord(*addr, hexCode); // We write the hex code, and advance the address counter
        //printf("Hex:%08X\n", hexCode);
        printf("   %08X %08X  {%s}\n", *addr, hexCode, entireInst);
        *addr += 4;
    }
}

void parseLine(FILE *file, void (*callback)(char *, int, int*, u_int32_t*, FILE*), FILE *output, u_int32_t *addr) {
    char *op = NULL;
    size_t len = 0;
    ssize_t c;

    int l = 0;

    while ((c = getline(&op, &len, file)) != -1) { // Let's treat labels.
        if (op[0] != '\n' && op[0] != '\r' && op[0] != '#') { // Remove empty lines and comments
            if (op[0] == '/' && op[1] == '*') { // Check for multiline comment
                int inComment = 1;
                while ((c = getline(&op, &len, file)) != -1 && inComment) {
                    if (op[0] == '*' && op[1] == '/') {
                        inComment = 0;
                    }
                }
            } else { // Not in comment, not an emptyline, that's an instruction or a label
                // Remove \n, and \r if needed
                int i = c-1;
                while (op[i] == '\n' || op[i] == '\r')
                    op[i--] = '\0';

                op = strtok(op, "#"); // Remove comments inline
                char *temp = op;

                while (temp[0] == ' ') // Remove leading spaces
                    temp++;

                int j = 0;
                while (temp[j] != '\0')
                    j++;

                while (temp[--j] == ' ') // Remove trailing spaces
                    temp[j] = '\0';

                // temp is formatted correctly, and contains only instructions or labels, with no trailing or leading spaces.
                // Here, i is the index of the last useful char. If it's a label, it does end with ':'.
                callback(temp, j, &l, addr, output);
                // We call the function passed in argument, to parse correctly labels and instructions
            }
        }
    }

    free(op);
}

void printRegisters() {
    printf("--Register states--\n");

    for (int i = 0; i < 32; i++) {
        printf("$%02d = %-8d ", i, registers[i]);

        if ((i+1)%4 == 0) {
            printf("\n");
        }
    }
    printf("HI: %u\nLO: %u\n", HI, LO);
}

void interactif() {
    initMemory();

    int exitCondition = 0;
    size_t size = 50;
    char buffer[size];
    while (!exitCondition) {
        printf("# ");
        fgets(buffer, size, stdin);

        int i = 0;
        while(buffer[i] != '\n')
            i++;
        buffer[i] = '\0';

        if (strcmp(buffer, "exit") == 0) {
            exitCondition = 1;
        } else if (strcmp(buffer, "r") == 0) {
            printRegisters();
        } else if (strcmp(buffer, "m") == 0) {
            printMemory();
        } else {
            // Execution de l'inst
            int status = 0;
            u_int32_t hexCode = parseStringToHex(buffer, &status, 0);
            if (status == 0) {
                fprintf(stderr, "Unsupported instruction : %s\n", buffer);
            } else {
                printf("Hexcode : %08X\n", hexCode);

                executeInstruction(hexCode);

                printRegisters();
            }
        }
    }
}

void programRead(int pas, char* fileName) {
    FILE *file = openFile(fileName, "r");
    char outputName[50] = "hexified/";
    strcat(outputName, fileName);
    printf("Output to : %s\n", outputName);
    FILE *output = openFile(outputName, "w");

    printf("Initiating memory...\n");
    initMemory();
    u_int32_t addr = PROG_OFFSET; // Let's say that the program start at 0x64

    // We do a first pass to save all the labels and their corresponding addresses
    parseLine(file, parseLabels, output, &addr);
    // Here, output is not necessary, but we need to have it because the other function needs it.

    rewind(file); // We reset the file pointer, to restart to read.
    addr = PROG_OFFSET; // we reset the address.

    // We parse instructions.
    parseLine(file, parseInstructions, output, &addr);

    fclose(output);
    fclose(file);

    printf("Starting execution...\n");

    PC = PROG_OFFSET;
    int maxAddr = addr; // As we wrote the program in memory, we have the address of the last insertion
    int cnt = 0;
    while (PC <= maxAddr && cnt < 500) {
        u_int32_t instruction = readWord(PC);

        if (pas) {
            int cont = 0;
            printf("Excecuting %08X at address %08X\n", instruction, PC);
            printf("[r]: display registers; [m]: memory; [c]: continue\n");
            char buffer;
            while (cont != 1) {
                scanf("%c", &buffer);

                if (buffer == 'r') {
                    printRegisters();
                } else if (buffer == 'm') {
                    printMemory();
                } else if (buffer == 'c') {
                    cont = 1;
                }
            }
        }

        executeInstruction(instruction);
        PC += 4;
        cnt++;
    }
    
    printRegisters();

    printf("Memory state\n");
    printMemory();

    freeMemory();
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        interactif();
    }

    if (argc >= 2) {
        if (argc >= 3 && strcmp(argv[2], "-pas") == 0) {
            programRead(1, argv[1]);
        } else {
            programRead(0, argv[1]);
        }
    }
}
