#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <stdint.h>
#include <math.h>

#include "instructions.h"
#include "cpu.h"
#include "memory.h"

void ADD(u_int8_t rd, u_int8_t rs, u_int8_t rt) {
    registers[rd] = registers[rs] + registers[rt];
}

void ADDI(u_int8_t rt, u_int8_t rs, int16_t immediate) {
    registers[rt] = registers[rs] + immediate;
}

void AND(u_int8_t rd, u_int8_t rs, u_int8_t rt) {
    registers[rd] = registers[rs] & registers[rt];
}

void BEQ(u_int8_t rs, u_int8_t rt, int16_t offset) {
    if (registers[rs] == registers[rt])
        PC += offset*4;
}

void BGTZ(u_int8_t rs, int16_t offset) {
    if (registers[rs] > 0)
        PC += offset*4;
}

void BLEZ(u_int8_t rs, int16_t offset) {
    if (registers[rs] <= 0)
        PC += offset*4;
}

void BNE(u_int8_t rs, u_int8_t rt, int16_t offset) {
    if (registers[rs] != registers[rt])
        PC += offset*4;
}

void DIV(u_int8_t rs, u_int8_t rt) {
    LO = registers[rs] / registers[rt];
    HI = registers[rs] % registers[rt];
}

void Jfunction(u_int32_t target) {
    PC = (target << 2 | (PC & (0b1111 << 28))) - 4;
}

void JAL(u_int32_t target) {
    registers[31] = PC - PROG_OFFSET;
    PC = (target << 2 | (PC & (0b1111 << 28))) - 4;
}

void JR(u_int8_t rs) {
    PC = registers[rs] + PROG_OFFSET; // offset début de prog
}

void LUI(u_int8_t rt, int16_t imm) {
    registers[rt] |= imm << 16;
}

void LW(u_int8_t rt, int16_t offset, u_int8_t base) {
    if (offset % 4 != 0) {
        fprintf(stderr, "Offset is not modulo 4, can't load.\n");
    } else {
        registers[rt] = readWord(registers[base] + offset);
    }
}

void MFHI(u_int8_t rd) {
    registers[rd] = HI;
}

void MFLO(u_int8_t rd) {
    registers[rd] = LO;
}

void MULT(u_int8_t rs, u_int8_t rt) {
    int64_t a = registers[rs];
    int64_t b = registers[rt];
    u_int32_t mask = 0xFFFFFFFF;
    LO = (a*b) & mask;
    HI = (a*b) >> 32 & mask;
}

void OR(u_int8_t rd, u_int8_t rs, u_int8_t rt) {
    registers[rd] = registers[rs] | registers[rt];
}

void ROTR(u_int8_t rd, u_int8_t rt, u_int8_t sa) {
    int32_t temp = registers[rt] & ((int) pow(2, sa) - 1);
    registers[rd] = (registers[rt] >> sa) | (temp << (32-sa));
}

void SLL(u_int8_t rd, u_int8_t rt, u_int8_t sa) {
    registers[rd] = registers[rt] << sa;
}

void SLT(u_int8_t rd, u_int8_t rs, u_int8_t rt) {
    registers[rd] = registers[rs] < registers[rt];
}

void SRL(u_int8_t rd, u_int8_t rt, u_int8_t sa) {
    registers[rd] = registers[rt] >> sa;
}

void SUB(u_int8_t rd, u_int8_t rs, u_int8_t rt) {
    if (registers[rs] < registers[rt]) {
        fprintf(stderr, "IntegerOverflow on SUB.\n");
    } else {
        registers[rd] = registers[rs] - registers[rt];
    }
}

void SW(u_int8_t rt, int16_t offset, u_int8_t base) {
    if (offset % 4 != 0) {
        fprintf(stderr, "Offset is not modulo 4, can't write.\n");
    } else {
        writeWord(registers[base] + offset, registers[rt]);
    }
}

void XOR(u_int8_t rd, u_int8_t rs, u_int8_t rt) {
    registers[rd] = registers[rs] ^ registers[rt];
}
