#ifndef INST_H
#define INST_H

void ADD(u_int8_t rd, u_int8_t rs, u_int8_t rt);
void ADDI(u_int8_t rs, u_int8_t rt, int16_t immediate);
void AND(u_int8_t rd, u_int8_t rs, u_int8_t rt);
void BEQ(u_int8_t rs, u_int8_t rt, int16_t offset);
void BGTZ(u_int8_t rs, int16_t offset);
void BLEZ(u_int8_t rs, int16_t offset);
void BNE(u_int8_t rs, u_int8_t rt, int16_t offset);
void DIV(u_int8_t rs, u_int8_t rt);
void Jfunction(u_int32_t target);
void JAL(u_int32_t target);
void JR(u_int8_t rs);
void LUI(u_int8_t rt, int16_t imm);
void LW(u_int8_t rt, int16_t offset, u_int8_t base);
void MFHI(u_int8_t rd);
void MFLO(u_int8_t rd);
void MULT(u_int8_t rs, u_int8_t rt);
void OR(u_int8_t rd, u_int8_t rs, u_int8_t rt);
void ROTR(u_int8_t rd, u_int8_t rt, u_int8_t sa);
void SLL(u_int8_t rd, u_int8_t rt, u_int8_t sa);
void SLT(u_int8_t rd, u_int8_t rs, u_int8_t rt);
void SRL(u_int8_t rd, u_int8_t rt, u_int8_t sa);
void SUB(u_int8_t rd, u_int8_t rs, u_int8_t rt);
void SW(u_int8_t rt, int16_t offset, u_int8_t base);
void XOR(u_int8_t rd, u_int8_t rs, u_int8_t rt);

#endif