#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <stdint.h>

#include "memory.h"

// Here, we'll make some helpers function to simulate 4GB of memory
// with a linked list

Element *memory;

void initMemory() {
    memory = malloc(sizeof(Element));
    memory->address = 0;
    memory->data = 0;
    memory->next = NULL;
}

int32_t readWord(u_int32_t address) {
    int32_t result = 0; // If we haven't wrote in the memory, we default to 0.
    Element *temp = memory;

    while (temp->next != NULL && temp->address < address) {
        temp = temp->next;
    }

    if (temp->address == address) {
        result = temp->data;
    }

    return result;
}

void writeWord(u_int32_t address, int32_t data) {
    if (address == 0) { // Can we find a better solution ?
        memory->data = data;
    } else {
        Element *temp = memory;

        while (temp->next != NULL && temp->next->address < address) {
            temp = temp->next;
        }

        // 2 cases :
        // - We are at the end of the list
        // - We want to insert here (bc next address is >= to the one we want)

        // We use lazy evaluation here
        if (temp->next != NULL && temp->next->address == address) {
            temp->next->data = data;
        } else {
            Element *after = temp->next; // NULL or address of next element, just before our insertion
            Element *new = malloc(sizeof(Element));
            new->address = address;
            new->data = data;
            new->next = after;
            temp->next = new;
        }
    }
}

// This function helps in debugging, it'll print all memory that was written at some point
void printMemory() {
    Element *temp = memory;
    u_int32_t oldAddr = 0;
    int wasLastReturn = 0;
    
    while (temp != NULL) {
        wasLastReturn = 0;
        for (; temp->address > oldAddr; oldAddr += 4) {
            printf("@%08X %08X   ", oldAddr, 0);
            if ((oldAddr+4) % 16 == 0) {
                printf("\n");
                wasLastReturn = 1;
            }
        }

        printf("@%08X %08X   ", temp->address, temp->data);

        if ((temp->address+4) % 16 == 0) {
            printf("\n");
            wasLastReturn = 1;
        }
        oldAddr = temp->address + 4;
        temp = temp->next;
    }

    if (!wasLastReturn) {
        printf("\n");
    }
}

void freeMemory() {
    Element *temp = memory;
    
    while (temp != NULL) {
        Element *toFree = temp;
        temp = temp->next;
        free(toFree);
    }
}
