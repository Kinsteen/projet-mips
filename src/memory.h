#ifndef MEMORY_H
#define MEMORY_H

/*

Nous avons décidé d'utiliser une liste chainé ordonnée croissante par l'adresse
pour simuler la mémoire du processeur.
Cette approche nous permet d'avoir une petite complexité spatiale si un programme
n'utilise pas beacoup de mémoire (que ce soit en taille du programme ou avec des
instructions d'écriture/lecture).
Les performances seront évidemment moins grande qu'avec un tableau direct, mais la place
gagnée est non négligeable devant un temps de calcul plus grand, mais toujours relativement
faible.

*/

typedef struct memoryStruct {
    int32_t data;
    u_int32_t address; // Need to be modulo 4.
    struct memoryStruct *next;
} Element;

void initMemory();
int32_t readWord(u_int32_t address);
void writeWord(u_int32_t address, int32_t data);
void printMemory();
void freeMemory();

#endif