#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <stdint.h>
#include <math.h>
#include <ctype.h>

#include "parser.h"

// First argument is number of element to shift to construct the hex number
u_int32_t generateOpcodeHexa(int arg_count, ...) {
    u_int32_t result = 0;

    va_list ap; // Initialise les arguments "dynamiques"

    va_start(ap, arg_count);

    for (int i = 0; i < arg_count; i++) {
        char *chain = va_arg(ap, char*);
        int mask = va_arg(ap, int);
        int offset = va_arg(ap, int);

        int converted = strtol(chain, NULL, 10) & ((int) pow(2, mask) - 1);
        result += converted << offset;
    }

    return result;
}

void replaceOffsetLabel(char *label, u_int32_t currentAddress) {
    if (!isdigit(label[0])) { // Check if it's a label or not
        int i = 0;
        int found = 0;
        label = strtok(label, " ");
        while (i < NB_LABELS && !found) {
            if (strcmp(labelArray[i].labelName, label) == 0) {
                char str[12];
                int nOffset = (labelArray[i].address - currentAddress);
                nOffset = (nOffset/4) - 1;
                sprintf(str, "%d", nOffset); // Convert int to char*
                strcpy(label, str); // Copy it to the offset
                found = 1; // Stops the loop
                
                //printf("label addr : %d\n", labelArray[i].address);
                //printf("curr : %d\n", currentAddress);
                //printf("BNE new offset : %d\n", nOffset);
            }

            i++;
        }
    }
}

u_int32_t parseStringToHex(char* op, int *status, u_int32_t currentAddress) {
    *status = 1;
    char *inst = strtok(op, " ");
    u_int32_t hexCode = 0;

    if (strcmp(inst, "ADD") == 0) {
        char *rd = strtok(NULL, ",") + 1; // To remove dollar sign
        char *rs = strtok(NULL, ",") + 1;
        char *rt = strtok(NULL, ",") + 1;

        hexCode = generateOpcodeHexa(4, rs,5,21, rt,5,16, rd,5,11, "32",6,0);
    } else if (strcmp(inst, "ADDI") == 0) {
        char *rt = strtok(NULL, ",") + 1;
        char *rs = strtok(NULL, ",") + 1;
        char *imm = strtok(NULL, ",");

        hexCode = generateOpcodeHexa(4, "8",6,26, rs,5,21, rt,5,16, imm,16,0);
    } else if (strcmp(inst, "AND") == 0) {
        char *rd = strtok(NULL, ",") + 1;
        char *rs = strtok(NULL, ",") + 1;
        char *rt = strtok(NULL, ",") + 1;

        hexCode = generateOpcodeHexa(4, rs,5,21, rt,5,16, rd,5,11, "36",6,0);
    } else if (strcmp(inst, "BEQ") == 0) {
        char *rs = strtok(NULL, ",") + 1;
        char *rt = strtok(NULL, ",") + 1;
        char *offset = strtok(NULL, ","); // Offset can be a numeric offset or a label

        replaceOffsetLabel(offset, currentAddress);

        hexCode = generateOpcodeHexa(4, "4",5,26, rs,5,21, rt,5,16, offset,16,0);
    } else if (strcmp(inst, "BGTZ") == 0) {
        char *rs = strtok(NULL, ",") + 1;
        char *offset = strtok(NULL, ","); // Offset can be a numeric offset or a label

        replaceOffsetLabel(offset, currentAddress);

        hexCode = generateOpcodeHexa(3, "7",6,26, rs,5,21, offset,16,0);
    } else if (strcmp(inst, "BLEZ") == 0) {
        char *rs = strtok(NULL, ",") + 1;
        char *offset = strtok(NULL, ","); // Offset can be a numeric offset or a label

        replaceOffsetLabel(offset, currentAddress);

        hexCode = generateOpcodeHexa(3, "6",6,26, rs,5,21, offset,16,0);
    } else if (strcmp(inst, "BNE") == 0) {
        char *rs = strtok(NULL, ",") + 1;
        char *rt = strtok(NULL, ",") + 1;
        char *offset = strtok(NULL, ","); // Offset can be a numeric offset or a label

        replaceOffsetLabel(offset, currentAddress);

        hexCode = generateOpcodeHexa(4, "5",6,26, rs,5,21, rt,5,16, offset,16,0);
    } else if (strcmp(inst, "DIV") == 0) {
        char *rs = strtok(NULL, ",") + 1;
        char *rt = strtok(NULL, ",") + 1;

        hexCode = generateOpcodeHexa(3, rs,5,21, rt,5,16, "26",6,0);
    } else if (strcmp(inst, "J") == 0) {
        char *target = strtok(NULL, ",");

        if (!isdigit(target[0])) { // Check if it's a label or not
            int i = 0;
            int found = 0;
            target = strtok(target, " ");
            while (i < NB_LABELS && !found) {
                if (strcmp(labelArray[i].labelName, target) == 0) {
                    char str[12];
                    int formattedAddr = labelArray[i].address / 4;
                    sprintf(str, "%d", formattedAddr); // Convert int to char*
                    strcpy(target, str); // Copy it to the offset
                    found = 1; // Stops the loop
                }

                i++;
            }
        }
        printf("New target : %s\n", target);

        hexCode = generateOpcodeHexa(2, "2",6,26, target,26,0);
    } else if (strcmp(inst, "JAL") == 0) {
        char *target = strtok(NULL, ",");

        if (!isdigit(target[0])) { // Check if it's a label or not
            int i = 0;
            int found = 0;
            target = strtok(target, " ");
            while (i < NB_LABELS && !found) {
                if (strcmp(labelArray[i].labelName, target) == 0) {
                    char str[12];
                    int formattedAddr = labelArray[i].address / 4;
                    sprintf(str, "%d", formattedAddr); // Convert int to char*
                    strcpy(target, str); // Copy it to the offset
                    found = 1; // Stops the loop
                }

                i++;
            }
        }
        printf("New target : %s\n", target);

        hexCode = generateOpcodeHexa(2, "3",6,26, target,26,0);
    } else if (strcmp(inst, "JR") == 0) {
        char *rs = strtok(NULL, ",") + 1;

        hexCode = generateOpcodeHexa(2, rs,5,21, "8",6,0);
    } else if (strcmp(inst, "LUI") == 0) {
        char *rt = strtok(NULL, ",") + 1;
        char *imm = strtok(NULL, ",");

        hexCode = generateOpcodeHexa(3, "15",6,26, rt,5,16, imm,16,0);
    } else if (strcmp(inst, "LW") == 0) {
        // Parsing is different because of parenthesis
        char *rt = strtok(NULL, ",") + 1;
        char *offset = strtok(strtok(NULL, ","),"(");
        char *base = strtok(NULL,")") + 1;

        hexCode = generateOpcodeHexa(4, "35",6,26, base,5,21, rt,5,16, offset,16,0);
    } else if (strcmp(inst, "MFHI") == 0) {
        char *rd = strtok(NULL, ",") + 1;

        hexCode = generateOpcodeHexa(2, rd,5,11, "16",6,0);
    } else if (strcmp(inst, "MFLO") == 0) {
        char *rd = strtok(NULL, ",") + 1;

        hexCode = generateOpcodeHexa(2, rd,5,11, "18",6,0);
    } else if (strcmp(inst, "MULT") == 0) {
        char *rs = strtok(NULL, ",") + 1;
        char *rt = strtok(NULL, ",") + 1;

        hexCode = generateOpcodeHexa(3, rs,5,21, rt,5,16, "24",6,0);
    } else if (strcmp(inst, "NOP") == 0) {
        hexCode = 0;
    } else if (strcmp(inst, "OR") == 0) {
        char *rd = strtok(NULL, ",") + 1;
        char *rs = strtok(NULL, ",") + 1;
        char *rt = strtok(NULL, ",") + 1;

        hexCode = generateOpcodeHexa(4, rs,5,21, rt,5,16, rd,5,11, "37",6,0);
    } else if (strcmp(inst, "ROTR") == 0) {
        char *rd = strtok(NULL, ",") + 1;
        char *rt = strtok(NULL, ",") + 1;
        char *sa = strtok(NULL, ",");

        hexCode = generateOpcodeHexa(5, "1",6,21, rt,5,16, rd,5,11, sa,5,6, "2",6,0);
    } else if (strcmp(inst, "SLL") == 0) {
        char *rd = strtok(NULL, ",") + 1;
        char *rt = strtok(NULL, ",") + 1;
        char *sa = strtok(NULL, ",");

        hexCode = generateOpcodeHexa(3, rt,5,16, rd,5,11, sa,5,6);
    } else if (strcmp(inst, "SLL") == 0) {
        char *rd = strtok(NULL, ",") + 1;
        char *rt = strtok(NULL, ",") + 1;
        char *sa = strtok(NULL, ",");

        hexCode = generateOpcodeHexa(3, rt,5,16, rd,5,11, sa,5,6);
    } else if (strcmp(inst, "SLT") == 0) {
        char *rd = strtok(NULL, ",") + 1;
        char *rs = strtok(NULL, ",") + 1;
        char *rt = strtok(NULL, ",") + 1;

        hexCode = generateOpcodeHexa(4, rs,5,21, rt,5,16, rd,5,11, "42",6,0);
    } else if (strcmp(inst, "SRL") == 0) {
        char *rd = strtok(NULL, ",") + 1;
        char *rt = strtok(NULL, ",") + 1;
        char *sa = strtok(NULL, ",");

        hexCode = generateOpcodeHexa(4, rt,5,16, rd,5,11, sa,5,6, "2",6,0);
    } else if (strcmp(inst, "SUB") == 0) {
        char *rd = strtok(NULL, ",") + 1;
        char *rs = strtok(NULL, ",") + 1;
        char *rt = strtok(NULL, ",") + 1;

        hexCode = generateOpcodeHexa(4, rs,5,21, rt,5,16, rd,5,11, "34",6,0);
    } else if (strcmp(inst, "SW") == 0) {
        char *rt = strtok(NULL, ",") + 1;
        char *offset = strtok(strtok(NULL, ","),"(");
        char *base = strtok(NULL,")") + 1;

        hexCode = generateOpcodeHexa(4, "43",6,26, base,5,21, rt,5,16, offset,16,0);
    } else if (strcmp(inst, "XOR") == 0) {
        char *rd = strtok(NULL, ",") + 1;
        char *rs = strtok(NULL, ",") + 1;
        char *rt = strtok(NULL, ",") + 1;

        hexCode = generateOpcodeHexa(4, rs,5,21, rt,5,16, rd,5,11, "38",6,0);
    } else {
        *status = 0;
    }

    return hexCode;
}