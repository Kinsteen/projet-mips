#ifndef PARSER_H
#define PARSER_H

#define NB_LABELS 50

struct label {
    char labelName[20];
    u_int32_t address;
};

struct label labelArray[NB_LABELS];

u_int32_t generateOpcodeHexa(int arg_count, ...);
u_int32_t parseStringToHex(char* op, int *status, u_int32_t currentAddress);

#endif